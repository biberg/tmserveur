package tm.util;

import java.nio.ByteBuffer;

/**
 * CLASSE COMMUNE AUX DEUX CÔTÉS (CLIENT ANDROID & SERVEUR PC)
 * Classe objet basique représentant un paquet tel qu'il est transmis entre client et serveur
 */
public class Packet {
    // ID du paquet; cf. enum interne plus bas
    private final byte ID;
    // ByteBuffer et non pas tableau de bytes! Important car dispose de fonctions plus pratiques pour la manipulation
    private final ByteBuffer DATA;

    public Packet(byte id, ByteBuffer data) {
        this.ID = id;
        this.DATA = data;
    }

    public byte getID() {
        return this.ID;
    }

    public ByteBuffer getData() {
        return this.DATA;
    }

    /**
     * Enum contenant tous les ordres ou instructions transmis par réseau.
     * Pour chaque élément, on a un ID et un RESPONSE_ID. L'ID est utilisé pour le paquet initial qui ordonne qqch (client -> serveur)
     * Le RESPONSE_ID est utilisé pour l'éventuelle réponse du serveur au client par rapport à un ordre précédent (serveur -> client)
     */
    public enum Packets {
        ERR((byte) 1), CONFIRMATION((byte) 2), LS((byte) 3), WELCOME((byte) 5), TRANSMIT_DATA((byte) 6), TRANSMIT_RES_DATA((byte) 7);

        public final byte ID;
        public final byte RESPONSE_ID;

        Packets(byte id) {
            this.ID = id;
            this.RESPONSE_ID = (byte)-id; // RESPONSE_ID est l'opposé d'ID (= -ID)
        }
    }
}
