package tm.util;

/**
 * Classe implémentant un mécanisme permettant de transmettre des données en toute sécurité dans un environnement multithreadé.
 * On peut verrouiller ou déverrouiller les instances de cette classe.
 * Est utilisé ainsi: lorsqu'on veut transmettre des données, on crée un objet de cette classe, on y insère les données puis on le verrouille.
 * Les données ne pourront donc être écrasées après que l'objet ait explicitement été déverrouillé, après les données traitées.
 */
public class ByteArrayLockingWrapper {
    byte[] data;
    boolean locked = false;

    public void lock() {
        this.locked = true;
    }

    public void unlock() {
        this.locked = false;
    }

    /**
     * Change les données contenues dans l'array d'octets (seulement si l'instance est déverrouillée).
     *
     * @param data données à insérer
     * @return true si on a pu changer les données, false sinon
     */
    public boolean setData(byte[] data) {
        if (this.locked) {
            return false;
        } else {
            this.data = data;

            return true;
        }
    }

    public byte[] getData() {
        return this.data;
    }
}
