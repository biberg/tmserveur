package tm.util;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * CLASSE COMMUNE AUX DEUX CÔTÉS (CLIENT ANDROID & SERVEUR PC)
 * Classe comportant différentes fonctions purement utilitaires à plusieurs parties du programme
 */
public class Util {
    public static boolean isClient = false; // sera utilisé pour déterminer le comportement à adopter lors de la déconnexion et lors de la fin inattendue de la connexion

    public static final byte[] MAGIC_WORD = {127, 127, 0, -127, 13, 25, 2, 4}; // envoyé et reçu comme vérification supplémentaire avant chaque paquet transmis

    /**
     * Éxecute le processus décrit par processBuilder.
     * Écoute le stdout (flux sortie classique) et le stderr (flux d'erreur) qui en sortent.
     * Retourne le tout dans une unique chaîne de caractères.
     * ATTENTION: BLOQUE LE THREAD COURANT JUSQU'À LA FIN DE L'EXÉCUTION
     *
     * @param processBuilder ProcessBuilder rempli correctement avec les informations du processus qu'on veut lancer
     * @return sortie du programme exécuté avec succès, null si erreur d'exécution ou de lecture
     */
    public static String runProcess(ProcessBuilder processBuilder) {
        try {
            processBuilder.redirectErrorStream(true); // on veut aussi recevoir les erreurs dans le string final

            Process process = processBuilder.start();
            process.waitFor();

            return new String(readNBytes(process.getInputStream(), process.getInputStream().available())); // lit la totalité des octets (caractères) disponibles
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Permet de changer le nom du fichier (situé à la fin de) path
     * Par exemple, avec path = "/home/gaston/bonjour.txt" et newFileName = "hallo.pdf":
     *      -> résultat: "/home/gaston/hallo.pdf"
     *
     * @param path chemin d'un fichier (on veut changer le nom du fichier désigné par ce chemin)
     * @param newFileName nom par lequel il faut remplacer la fin de path
     * @return chemin modifié
     */
    public static String changeFileName(String path, String newFileName) {
        int lastSlash = path.lastIndexOf('/');

        String pathToLastFolder = path.substring(0, lastSlash + 1);
        String newPath = pathToLastFolder + newFileName;

        return newPath;
    }

    /**
     * Permet d'emballer un tableau de strings en un array d'octets, très facilement transmissible sur le réseau
     *
     * @param array array de strings que l'on veut "empaqueter" dans un array d'octets
     * @return array d'octets contenant tous les strings
     */
    public static byte[] getBytesFromStrings(String[] array) {
        // n'est pas réellement un flux sortant en dehors du programme, tout ce qui y est écrit reste en mémoire puis est récupérable avec la fonction toByteArray()
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        for (String str : array) {
            byte[] stringBytes = str.getBytes(StandardCharsets.UTF_8);

            int length = stringBytes.length; // !!! pas nécessairement égal à str.length() (caractères spéciaux!) ca ma pris tellement de temps de trouver cette erreur!!!!!
            ByteBuffer lengthByteBuffer = ByteBuffer.allocate(4).putInt(length); // permet de convertir un entier de 32 bits en un buffer (tableau) de 4 octets

            byte[] lengthBytes = lengthByteBuffer.array();

            try {
                byteArrayOutputStream.write(lengthBytes);
                byteArrayOutputStream.write(stringBytes);
            } catch (IOException e) {
                e.printStackTrace(); // ne devrait jamais arriver avec un ByteArrayOutputStream
            }
        }

        return byteArrayOutputStream.toByteArray();
    }

    /**
     * Permet de décoder un tableau d'octets créé par getBytesFromStrings(...)
     *
     * @param array tableau d'octets créé par getBytesFromStrings()
     * @param quantity nombre de strings à extraire
     * @return tableau contenant les strings récupérés
     */
    public static String[] getStringsFromBytes(byte[] array, int quantity) {
        List<String> stringList = new ArrayList<>();

        int offset = 0;

        for (int i = 0; i < quantity; i++) {
            byte[] stringLengthBytes = Arrays.copyOfRange(array, offset, offset += 4);
            int stringLength = ByteBuffer.wrap(stringLengthBytes).getInt();

            byte[] stringBytes = Arrays.copyOfRange(array, offset, offset += stringLength);
            String decodedString = new String(stringBytes, StandardCharsets.UTF_8);

            stringList.add(decodedString);
        }

        String[] strings = new String[stringList.size()];
        strings = stringList.toArray(strings);

        return strings;
    }

    /**
     * Si str est vide, on retourne un "0", sinon on retourne le string lui-même.
     *
     * @param str chaîne à contrôler
     * @return "0" si str est vide, str lui-même sinon
     */
    public static String nullIfEmpty(String str) {
        return str.isEmpty() ? "0" : str;
    }

    /**
     * Lit n octets à partir du flux inputStream.
     * Échoue en cas de fermeture / fin du flux au milieu de la lecture:
     * NOTE: une fonction similaire existe déjà dans les classes standard Java pour ordinateur, mais pas sur Android.
     *       Comme ma classe est commune aux deux appareils, j'ai du réécrire une implémentation moi-même.
     * TRES IMPORTANT https://wiki.sei.cmu.edu/confluence/display/java/FIO10-J.+Ensure+the+array+is+filled+when+using+read%28%29+to+fill+an+array
     *
     * @param inputStream flux d'entrée à partir duquel les octets doivent être lus
     * @param n nombre d'octets à lire à partir de inputStream
     * @return tableau de n octets rempli avec les données si l'opération réussit (et en plus, chez le client: null si une erreur survient)
     */
    public static byte[] readNBytes(InputStream inputStream, int n) throws IOException {
        byte[] bytes = new byte[n];

        int count = 0;
        while (count < n) { // on veut lire n octets, à chaque octet lu "count" est incrémenté
            // la fonction read() retourne le nombre d'octets lus. Ce nombre peut être, mais n'est pas forcément égal, à la longueur demandée. Cela n'est notamment pas le cas si les données sont transmises par le réseau sur plusieurs paquets TCP différents, ce qui est souvent le cas dans mon projet.
            // on doit donc se souvenir de la quantité de données lues pour pouvoir reprendre la lecture au bon endroit pour le prochain morceau, le cas échéant.
            // d'où read(bytes, count, n - count) où bytes est l'array de destination, count est le décalage qu'il faut prendre en compte (= qté de données déjà lues) et n - count est la quantité d'octets restant à lire.
            int readBytes = inputStream.read(bytes, count, n - count);

            if (readBytes == -1) { // une quantité lue de -1 signifie que le flux a été interrompu (= erreur)
                System.out.println("Déconnecté");

                if (isClient) {
                    return null;
                } else {
                    System.exit(0);
                }
            } else {
                count += readBytes;
            }
        }

        return bytes;
    }

    /**
     * Envoit un paquet de communication sur outputStream:
     *      envoit tout d'abord un mot magique pour s'assurer que les deux côtés sont sur la même longueur d'onde..
     *      ensuite, envoit l'ID du paquet (cf. enum interne Packets dans tm.util.Packet)
     *      envoit ensuite la taille des données qui vont être envoyées (en octets)
     *      finalement, envoit les données de la taille citée précédemment.
     * Vérifie ensuite que le paquet a bien été réceptionné:
     *      écoute sur inputStream pour la confirmation du statut du paquet (reçu?, erreur?, ...)
     *
     * @param packet paquet à envoyer à l'autre côté
     * @param inputStream InputStream utilisé pour recevoir la confirmation que le paquet a été reçu de l'autre côté
     * @param outputStream OutputStream utilisé pour envoyer les données relatives au paquet
     * @return true si on reçoit la confirmation que le paquet a été envoyé, false sinon
     */
    public static boolean sendPacket(Packet packet, InputStream inputStream, OutputStream outputStream) {
        try {
            // on utilise un BufferedOutputStream car on va écrire plusieurs fois des petites quantités. Pas besoin de surcharger le réseau avec plein de petits transferts...
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);

            bufferedOutputStream.write(MAGIC_WORD); // envoie le "mot magique", qui sert de vérification par rapport à l'intégrité des données reçues
            bufferedOutputStream.write(packet.getID());
            bufferedOutputStream.write(ByteBuffer.allocate(4).putInt(packet.getData().array().length).array()); // envoie la longueur des données qui vont suivre
            bufferedOutputStream.write(packet.getData().array());

            //System.out.println("Envoi de " + packet.getData().array().length);

            // nécessaire de flush car on écrit vers un flux avec tampon (BufferedOutputStream)
            bufferedOutputStream.flush();

            if (packet.getID() != Packet.Packets.ERR.ID && packet.getID() != Packet.Packets.CONFIRMATION.ID && packet.getID() >= 0) { // on attend une réponse de l'autre côté uniquement si on n'est pas en train de justement envoyer un paquet de réponse (sinon on aurait une boucle infinie, il faut bien qu'on s'arrête à un moment)
                Packet response = receivePacket(inputStream);
                if (response.getID() == Packet.Packets.ERR.ID || (response.getID() != Packet.Packets.CONFIRMATION.ID && response.getID() != Packet.Packets.ERR.ID)) { // si on reçoit un paquet "erreur", ou un paquet autre que "erreur" ou "confirmation" (qui n'a pas sa place à cet endroit du protocole)
                    System.out.println("Erreur de l'autre côté!");
                    return false;
                }
            }

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Reçoit un paquet et le retourne en cas de succès:
     *      reçoit 8 octets, vérifie l'intégrité du mot magique
     *      lit l'ID du paquet
     *      IMPORTANT lit la longueur des données à recevoir
     *      -> lit EXACTEMENT le bon nombre d'octets (seule manière de savoir quand s'arrêter! galère sans savoir la taille)
     *
     * @param inputStream flux d'entrée à partir duquel on va tenter de recevoir un paquet
     * @return paquet reçu en cas de succès; null en cas d'erreur
     */
    public static Packet receivePacket(InputStream inputStream) {
        try {
            if (!Arrays.equals(readNBytes(inputStream, 8), MAGIC_WORD)) { // condition: les premiers 8 octets lus correspondent-ils?
                if (isClient) {
                    return null;
                } else {
                    System.out.println("Erreur de communication: format inattendu");

                    System.exit(1);
                }
            }

            byte packetID = (byte) inputStream.read();

            int dataLength = ByteBuffer.wrap(readNBytes(inputStream, 4)).getInt(); // on reçoit les 4 octets composant l'entier de 32 bits "longueur des données qui vont suivre" puis on les re-convertit en int

            byte[] packetData = readNBytes(inputStream, dataLength);

            //System.out.println("Paquet id " + packetID + " lu (" + dataLength + ")");
            return new Packet(packetID, ByteBuffer.wrap(packetData));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
