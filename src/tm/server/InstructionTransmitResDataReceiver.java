package tm.server;

import tm.util.Util;

import java.nio.ByteBuffer;

public class InstructionTransmitResDataReceiver extends Instruction {
    private final ByteBuffer dataBuffer;

    // Données qui seront remplies par celles reçues du client
    private static String lastTexPath = null;
    private static String lastResName = null;
    private static String lastFigureCaption = null;
    private static String lastIncludeGraphicsParameters = null;
    private static int lastFigureWidth = 0;
    private static int lastFigureHeight = 0;

    // verrou pour éviter que les données soient écrasées alors qu'on n'a pas fini de les traiter (si locked==true, les valeurs ci-dessus ne peuvent pas être changées)
    private static boolean locked = false;

    public InstructionTransmitResDataReceiver(ByteBuffer data, TMServer server) {
        super(data, server);

        this.dataBuffer = data;
    }

    @Override
    public Instruction autoHandle() {
        this.run();
        this.sendResponse();

        return this;
    }

    /**
     * Extrait 6 strings du l'array d'octets brut avec la fonction Util.getStringsFromBytes().
     * (de l'autre côté, le tableau d'octets a justement été fabriqué par la fonction réciproque Util.getBytesFromStrings())
     * Ensuite, stocke les données reçues dans les variables statiques déclarées en début de classe et vérouille pour protéger le tout.
     *
     * @return instance de l'instruction
     */
    @Override
    public Instruction run() {
        byte[] receivedDataArray = dataBuffer.array();

        String[] strings = Util.getStringsFromBytes(receivedDataArray, 6);

        if (!locked) {
            lastTexPath = strings[0];
            lastResName = strings[1];
            lastFigureCaption = strings[2];
            lastIncludeGraphicsParameters = strings[3];
            lastFigureWidth = Integer.parseInt(strings[4]);
            lastFigureHeight = Integer.parseInt(strings[5]);

            /*
            System.out.println("Chemin changé! " + lastTexPath);
            System.out.println("Nom changé! " + lastResName);
            System.out.println("Légende changée! " + lastFigureCaption);
            System.out.println("Paramètres changés! " + lastIncludeGraphicsParameters);
            System.out.println("Largeur changée! " + lastFigureWidth + " mm");
            System.out.println("Hauteur changée! " + lastFigureHeight + " mm");
            */

            locked = true;
        }

        return this;
    }

    /**
     * Envoie une simple confirmation au client.
     *
     * @return instance de l'instruction
     */
    @Override
    public Instruction sendResponse() {
        return super.sendResponse();
    }

    // les 6 fonctions suivantes servent uniquement à permettre un accès en lecture seule aux variables statiques de début de classe.

    public static String getLastTexPath() {
        return lastTexPath;
    }

    public static String getLastResName() {
        return lastResName;
    }

    public static String getLastFigureCaption() {
        return lastFigureCaption;
    }

    public static String getLastIncludeGraphicsParameters() {
        return lastIncludeGraphicsParameters;
    }

    public static int getLastFigureWidth() {
        return lastFigureWidth;
    }

    public static int getLastFigureHeight() {
        return lastFigureHeight;
    }

    /**
     * Remet toutes les variables à zéro et débloque le tout.
     * Après l'exécution de cette fonction, on est de nouveau prêts pour recevoir de nouvelles données.
     */
    public static void resetEverything() {
        lastTexPath = null;
        lastResName = null;
        lastFigureCaption = null;
        lastIncludeGraphicsParameters = null;
        lastFigureWidth = 0;
        lastFigureHeight = 0;

        locked = false;
    }
}
