package tm.server;

import tm.util.Util;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

public class InstructionTransmitDataReceiver extends Instruction {
    private final ByteBuffer rawData;

    public InstructionTransmitDataReceiver(ByteBuffer data, TMServer server) {
        super(data, server);
        this.rawData = data;
    }

    @Override
    public Instruction autoHandle() {
        this.run();
        this.sendResponse();
        return this;
    }

    /**
     * Écrit les données de l'image reçue (contenues dans rawData) dans un fichier adapté.
     *
     * @return instance de l'instruction
     */
    @Override
    public Instruction run() {
        byte[] data = rawData.array();

        String filePath = InstructionTransmitResDataReceiver.getLastTexPath(); // dernier chemin transmis par le client
        String fileName = "res_" + InstructionTransmitResDataReceiver.getLastResName(); // dernier nom de fichier pour la ressource transmis par le client
        String fileExtension = LatexIntegration.getImageFormat(Arrays.copyOfRange(data, 0, 8)); // extension de fichier trouvée à partir de l'en-tête des données brutes de l'image (8 premiers octets)

        // crée le chemin vers la future image en remplaçant le nom du fichier .tex dans filePath par le nom de mon image => on se retrouve avec l'image enregistrée dans le même dossier que le document tex
        String resourceImagePath = Util.changeFileName(filePath, fileName + "." + fileExtension);


        try {
            Files.write(Path.of(resourceImagePath), data);
        } catch (IOException e) {
            e.printStackTrace();
        }

        LatexIntegration.integrate(); // modifie le document tex pour y intégrer l'image téléchargée.

        InstructionTransmitResDataReceiver.resetEverything(); // on remet les valeurs internes à zéro pour éviter toute confusion

        return this;
    }

    /**
     * Envoie une simple confirmation au client.
     *
     * @return instance de l'instruction
     */
    @Override
    public Instruction sendResponse() {
        return super.sendResponse();
    }
}
