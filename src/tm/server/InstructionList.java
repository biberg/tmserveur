package tm.server;

import tm.util.Packet;
import tm.util.Util;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class InstructionList extends Instruction {
    private final String DIRECTORY;
    private String result;

    public InstructionList(String directory, TMServer server) {
        super(directory, server);
        this.DIRECTORY = directory;
    }

    @Override
    public InstructionList autoHandle() {
        this.run();
        this.sendResponse();
        return this;
    }

    /**
     * Éxecute ls avec les paramètres:
     *  -a: inclure tous les fichiers (cachés y compris) et
     *  --file-type: ajoute un slash à la fin de chaque dossier (nous permettra de différencier fichiers et dossiers)
     *
     * @return instance de l'instruction
     */
    @Override
    public InstructionList run() {
        this.result = Util.runProcess(new ProcessBuilder("ls", "-a", "--file-type", this.DIRECTORY));
        return this;
    }

    /**
     * On envoit, en plus de la confirmation habituelle, le résultat de notre commande "ls".
     *
     * @return instance de l'instruction
     */
    @Override
    public InstructionList sendResponse() {
        super.sendResponse();
        try {
            getServer().sendPacket(new Packet(Packet.Packets.LS.RESPONSE_ID, ByteBuffer.wrap(this.result.getBytes(StandardCharsets.UTF_8))));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return this;
    }
}
