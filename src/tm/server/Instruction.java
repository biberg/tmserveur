package tm.server;

import tm.util.Packet;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Classe abstraite décrivant la structure basique d'une instruction.
 * Permet facilement d'exécuter n'importe quelle instance de sous-classe (= instruction concrète) facilement, en appelant run() et sendResponse().
 */
public abstract class Instruction {
    private final TMServer SERVER;

    public Instruction(Object o, TMServer server) {
        this.SERVER = server;
    }

    /**
     * S'occupe des actions contenues dans l'instruction de A à Z (doit donc être utilisé seul).
     * Concrètement: exécute l'instruction et envoie la réponse / confirmation au client.
     *
     * @return instance de l'instruction
     */
    public abstract Instruction autoHandle();

    /**
     * Éxecute les actions *localement* uniquement.
     *
     * @return instance de l'instruction
     */
    public abstract Instruction run();

    /**
     * Envoie le statut / la confirmation / le résultat de l'éxecution de l'instruction au client.
     *
     * @return instance de l'instruction
     */
    public Instruction sendResponse() {
        try {
            getServer().sendPacket(new Packet(Packet.Packets.CONFIRMATION.ID, ByteBuffer.allocate(0)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return this;
    }

    /**
     * @return instance du serveur
     */
    protected final TMServer getServer() {
        return this.SERVER;
    }

    /**
     * Retourne l'instruction correspondant à packet.
     * Ainsi, on n'a plus qu'à exécuter l'instruction retournée et c'est fini.
     *
     * @param packet paquet à partir duquel on veut extraire une instruction
     * @return l'instruction correspondante si l'ID du paquet est connu, sinon null
     */
    public static final Instruction getInstruction(Packet packet) {
        return packet.getID() == Packet.Packets.LS.ID ? new InstructionList(new String(packet.getData().array()), Main.getServer())
                : packet.getID() == Packet.Packets.TRANSMIT_DATA.ID ? new InstructionTransmitDataReceiver(packet.getData(), Main.getServer())
                : packet.getID() == Packet.Packets.TRANSMIT_RES_DATA.ID ? new InstructionTransmitResDataReceiver(packet.getData(), Main.getServer())
                : null;
    }
}
