package tm.server;

import java.io.*;
import java.util.Arrays;

public class LatexIntegration {
    // Représente la chaîne de caractères d'«accroche» qui sera remplacée par l'image dans le document .tex
    public static final String TO_BE_REPLACED = "$$$RESSOURCE$$$";

    // Code d'intégration qui va être inséré dans le document .tex (après remplacement des champs).
    public static final String[] INTEGRATION_CODE = {
            "\\begin{figure}[h]",
            "\\centering",
            "\\includegraphics[$$$PARAMETERS$$$]{$$$FILENAME$$$}",
            "\\caption{$$$CAPTION$$$}",
            "\\end{figure}"
    };

    // Ligne ajoutée au cas où l'utilisateur n'aurait pas encore graphicx pour son fichier (nécessaire pour \includegraphics{}).
    public static final String USE_PACKAGE_GRAPHCIX = "\\usepackage{graphicx}\n";

    private static BufferedReader bufferedFileReader = null;
    private static BufferedWriter bufferedFileWriter = null;

    // Le fichier source est lu, les changements nécessaires sont effectués -> le résultat se retrouve tout d'abord ici avant d'être écrit sur le disque.
    private static String newContent = "";

    /**
     * S'occupe de l'intégration avec toutes les données statiques de InstructionTransmitResDataReceiver
     */
    public static void integrate() {
        try {
            newContent = ""; // important de reset!! sinon, le code s'ajouterait à chaque fois à chaque transfert effectué par l'utilisateur...

            openFileRead();

            scanAndCompletePreamble();
            insertCode();

            bufferedFileReader.close(); // le Reader doit être fermé avant d'ouvrir le fichier en écriture

            //System.out.println(newContent);

            openFileWrite();

            bufferedFileWriter.write(newContent);
            bufferedFileWriter.flush();

            bufferedFileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Ouvre le fichier .tex en lecture seule dans un premier temps.
     * On écrit ensuite le fichier "modifié" dans la chaine de caractères newContent.
     * Dans un deuxième temps seulement, celle-ci sera écrite dans le fichier, *une fois le Reader fermé*. cf integrate() openFileWrite()
     */
    public static void openFileRead() throws IOException {
        String filePath = InstructionTransmitResDataReceiver.getLastTexPath();
        File file = new File(filePath);

        FileReader fileReader = new FileReader(file);
        // BufferedReader pour ne pas devoir lire octet par octet, mais ligne par ligne (éviter une surcharge inutile du disque)
        bufferedFileReader = new BufferedReader(fileReader);
    }

    /**
     * Ouvre le fichier .tex en lecture dans un deuxième temps.
     * Doit seulement être appelé quand on veut vraiment écrire dans le fichier, c. à. d. une fois que newContent est dans sa version finale.
     * Le Writer doit impérativement être flushé et fermé une fois les opérations terminées. cf integrate()
     */
    public static void openFileWrite() throws IOException {
        String filePath = InstructionTransmitResDataReceiver.getLastTexPath();
        File file = new File(filePath);

        FileWriter fileWriter = new FileWriter(file);
        // on utilise un BufferedWriter pour optimiser les écritures (sinon, serait inutilement demandeur en ressources disque)
        bufferedFileWriter = new BufferedWriter(fileWriter);
    }

    /**
     * Scanne le préambule du fichier .tex à la recherche de graphicx.
     * Si on le trouve: aucune modification nécessaire.
     * Si on ne le trouve pas:
     *      soit il n'y a aucun autre package utilisé, et on ajoute USE_PACKAGE_GRAPHICX avant \begin{document}
     *      soit il y a déjà des packages utilisés, et on ajoute graphicx avant le premier d'entre eux.
     * Cette fonction écrit son résultat dans la variable newContent (ajout à la fin de la variable).
     *
     * @throws IOException en cas d'erreur de lecture
     */
    public static void scanAndCompletePreamble() throws IOException {
        StringBuilder preamble = new StringBuilder(); // préambule auquel on ajoute chaque ligne lue au fur et à mesure (sans modifications à ce stade!)
        String finalPreamble = ""; // préambule final, modifié. (concrètement: avec \\usepackage{graphicx} ajouté si nécessaire)

        boolean beginDocumentFound = false; // \\begin{document} trouvé?
        boolean usePackageGraphicxFound = false; // \\usepackage[...]{graphicx} trouvé?
        boolean firstUsePackageFound = false; // \\usepackage quelconque trouvé?

        int lineCount = 0;
        int firstUsePackageIndex = -1; // c'est à cet endroit que sera inseré \\usepackage{graphicx} si une ligne \\usepackage est présente, mais pas graphicx.

        while (lineCount < 4096) { // pour éviter une boucle infinie s'il s'avère que le fichier lu ne contient aucune ligne avec \\begin{document}.
            String tmp = bufferedFileReader.readLine() + "\n"; // avec BufferedReader, la ligne lue ne comporte jamais le retour à la ligne -> on doit l'ajouter manuellement
            //System.out.println("lineCount=" + lineCount + " tmp=" + tmp); // debug

            // À quel stade on se trouve avant l'ajout de la ligne lue.
            int lengthBeforeAppend = preamble.length();

            preamble.append(tmp);

            if (tmp.contains("\\usepackage")) {
                if (!firstUsePackageFound) {
                    firstUsePackageFound = true;

                    firstUsePackageIndex = lengthBeforeAppend;
                }

                if (tmp.contains("{graphicx}")) {
                    usePackageGraphicxFound = true;
                }
            } else if (tmp.contains("\\begin{document}")) {
                beginDocumentFound = true;

                if (!usePackageGraphicxFound) {
                    if (!firstUsePackageFound) {
                        String preambleBeginning = preamble.substring(0, lengthBeforeAppend); // partie du préambule du début du fichier jusqu'à \\begin{document} non inclus

                        finalPreamble = preambleBeginning + USE_PACKAGE_GRAPHCIX + "\n" + tmp;
                    } else {
                        String preambleBeginning = preamble.substring(0, firstUsePackageIndex); // préambule du début jusqu'à la première ligne \\usepackage (non incluse)
                        String preambleEnd = preamble.substring(firstUsePackageIndex); // préambule de la première ligne \\usepackage (incluse) jusqu'à \\begin{document} (inclus)

                        finalPreamble = preambleBeginning + USE_PACKAGE_GRAPHCIX + preambleEnd;
                    }
                } else {
                    finalPreamble = preamble.toString(); // si graphicx est déjà présent, on n'apporte aucune modification
                }

                break;
            }

            lineCount++;
        }

        newContent += finalPreamble;
    }

    /**
     * Scanne le corps du document .tex à la recherche de TO_BE_REPLACED (=mot-clef).
     * On le remplace ensuite par le code d'intégration complété.
     * Si on ne trouve pas TO_BE_REPLACED, le corps du document n'est pas modifié.
     * Cette fonction écrit son résultat dans newContent (append).
     *
     * @throws IOException en cas d'erreur de lecture
     */
    public static void insertCode() throws IOException {
        boolean keywordFound = false;

        String st;
        while ((st = bufferedFileReader.readLine()) != null) { // jusqu'à la fin du fichier
            String s = st + "\n";
            if (s.contains(TO_BE_REPLACED)) {
                String indentation = ""; // dans cette variable sera contenue l'indentation utilisée avant le mot-clef, qui sera réinsérée devant chaque ligne ajoutée par la suite
                for (char c : s.toCharArray()) { // on parcourt la ligne jusqu'au premier caractère qui n'est pas un espace.
                    if (Character.isWhitespace(c)) {
                        indentation += c;
                    } else {
                        break;
                    }
                }

                s = "\n";
                for (String line : INTEGRATION_CODE) {
                    if (!line.isEmpty()) {
                        String completedLine = fillData(line);

                        s += indentation;
                        s += completedLine;
                        s += "\n";
                    }
                }
            }

            newContent += s;
        }
    }

    /**
     * Est prévu pour remplacer tous les champs à remplacer dans le code d'insertion INTEGRATION_CODE (paramètres includegraphics, légende, nom du fichier).
     *
     * @param blank ligne à scanner pour insérer les données correspondantes
     * @return ligne complétée
     */
    public static String fillData(String blank) {
        String includeGraphicsParameters = "";

        // ces trois premières conditions servent juste à créer le string de paramètres qui sera donné à \\includegraphics
        if (InstructionTransmitResDataReceiver.getLastFigureWidth() != 0) {
            String parameter = "width=" + InstructionTransmitResDataReceiver.getLastFigureWidth() + "mm";

            includeGraphicsParameters += parameter;
        }

        if (InstructionTransmitResDataReceiver.getLastFigureHeight() != 0) {
            String separator = includeGraphicsParameters.isEmpty() ? "" : ","; // si un paramètre est déjà présent il faut une virgule pour séparation

            String parameter = separator + "height=" + InstructionTransmitResDataReceiver.getLastFigureHeight() + "mm";

            includeGraphicsParameters += parameter;
        }

        if (!InstructionTransmitResDataReceiver.getLastIncludeGraphicsParameters().isEmpty()) {
            String separator = includeGraphicsParameters.isEmpty() ? "" : ",";

            String parameter = separator + InstructionTransmitResDataReceiver.getLastIncludeGraphicsParameters();

            includeGraphicsParameters += parameter;
        }

        String result = blank
                .replaceAll("\\$\\$\\$PARAMETERS\\$\\$\\$", includeGraphicsParameters)
                .replaceAll("\\$\\$\\$CAPTION\\$\\$\\$", InstructionTransmitResDataReceiver.getLastFigureCaption())
                .replaceAll("\\$\\$\\$FILENAME\\$\\$\\$", "res_" + InstructionTransmitResDataReceiver.getLastResName());

        return result;
    }

    /**
     * Tente de trouver le format de l'image récupérée à l'aide de l'en-tête du fichier.
     * Source pour les suites d'octets: observations personnelles.
     * Plus précisément, j'ai utilisé l'outil "od" pour examiner les premiers octets d'une grande quantité de fichiers et en ai tiré les conclusions suivantes.
     *
     * @param header_bytes premiers octets du fichier à analyser (au moins 8)
     * @return extension du format image détecté
     */
    public static String getImageFormat(byte[] header_bytes) {
        return Arrays.equals(Arrays.copyOfRange(header_bytes, 0, 3), new byte[]{-1, -40, -1}) ? "jpg"
                : Arrays.equals(Arrays.copyOfRange(header_bytes, 0, 8), new byte[]{-119, 80, 78, 71, 13, 10, 26, 10}) ? "png"
                : "img";
    }
}
