package tm.server;

import tm.util.Packet;
import tm.util.Util;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class TMServer {

    private final ServerSocket serverSocket;
    private Socket socket;

    /**
     * Ouvre un socket serveur sur le port spécifié en paramètre.
     *
     * @param port port sur lequel le serveur sera ouvert aux connexions entrantes
     * @throws IOException
     */
    public TMServer(int port) throws IOException {
        this.serverSocket = new ServerSocket(port);
    }

    /**
     * Envoie un paquet au client.
     *
     * @param packet paquet à envoyer
     * @return true si le paquet a bien été réceptionné par le client, false sinon
     * @throws IOException en cas d'erreur réseau
     */
    public boolean sendPacket(Packet packet) throws IOException {
        return Util.sendPacket(packet, this.socket.getInputStream(), this.socket.getOutputStream());
    }

    /**
     * Attend et réceptionne un paquet envoyé par le client.
     *
     * @return paquet reçu (envoyé par le client)
     * @throws IOException en cas d'erreur réseau
     */
    public Packet receivePacket() throws IOException {
        return Util.receivePacket(this.socket.getInputStream());
    }

    /**
     * Tente d'établir une connexion avec un client.
     * Réceptionne le premier paquet (envoyé par le client). Ce paquet doit contenir le bon code pour établir la connexion. Si le code est bon, on envoie la confirmation au client que la connexion s'est bien passée.
     *
     * @return true si la connection réussit, false sinon
     */
    public boolean connect() {
        try {
            this.socket = this.serverSocket.accept();

            Packet greeting = receivePacket(); // on reçoit le premier paquet de la "discussion" (qui doit normalement contenir le code de connexion)

            sendPacket(new Packet(Packet.Packets.CONFIRMATION.ID, ByteBuffer.allocate(0)));

            // on vérifie que le paquet reçu est bien le paquet de bienvenue et que le code transmis est correct. Le cas échéant on retourne true
            if (greeting.getID() == Packet.Packets.WELCOME.ID && new String(greeting.getData().array(), StandardCharsets.UTF_8).equals(Main.CODE)) {
                sendPacket(new Packet(Packet.Packets.WELCOME.RESPONSE_ID, ByteBuffer.wrap("hi".getBytes(StandardCharsets.UTF_8))));
                return true;
            }

            // sinon, on indique qu'une erreur est survenue lors de la connexion.
            return false;
        } catch (IOException e) { // en cas d'erreur réseau
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Écoute sur le réseau et attend le prochain paquet. Une fois le paquet réceptionné, utilise Instruction.getInstruction(packet) pour récupérer l'instruction à exécuter correspondante.
     *
     * @return instruction envoyée par le client
     */
    public Instruction listen() {
        try {
            return Instruction.getInstruction(this.receivePacket());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Retourne l'adresse IPv4 depuis laquelle le client s'est connecté.
     *
     * @return
     */
    public InetAddress getClientAddress() {
        return this.socket.getInetAddress();
    }
}
