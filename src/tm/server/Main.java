package tm.server;

import java.io.IOException;
import java.net.Inet4Address;
import java.util.concurrent.ThreadLocalRandom;

public class Main {
    public static final boolean DEBUG = false; // si true, certaines fonctionnalités du programme sont modifiées pour faciliter les tests.
    private static TMServer server;
    private static final int PORT = 8424; // port choisi arbitrairement dans une zone libre (peu utilisée)
    private static final int CODE_INT = DEBUG ? 123456 : ThreadLocalRandom.current().nextInt(1000000); // genère un code au hasard
    public static final String CODE = "0".repeat(6 - Integer.toString(CODE_INT).length()) + CODE_INT; // CODE_INT formatté proprement

    /**
     * Fonction principale. Lance le serveur.
     * Si la connexion échoue, sortie (code erreur 2).
     * Si la connexion réussit, on lance une boucle qui écoute les instructions envoyées par le client Android.
     *
     * @param args arguments ignorés
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        System.out.println("Adresse: " + Inet4Address.getLocalHost().getHostAddress() + ":" + PORT);

        server = new TMServer(PORT); // à partir de ce moment, le client peut initier une connexion vers le serveur.

        System.out.println("Code de connexion: " + CODE);

        if (server.connect()) { // si la connexion réussit
            System.out.println(server.getClientAddress() + " connecté!");
        } else { // si la connexion échoue
            System.exit(2);
        }

        Instruction result;
        while ((result = server.listen()) != null) { // chaque instruction envoyée par le client Android = un tour de boucle.
            // effectue l'instruction et envoie la réponse au client.
            // result est en pratique non pas un objet Instruction pur (d'ailleurs impossible car classe abstraite), mais une instance d'une instruction spécifique (voir Instruction.getInstruction())
            result.autoHandle();
        }
    }

    /**
     * Retourne une instance du serveur, qui peut être utilisée partout dans le programme pour la communication avec le client.
     *
     * @return instance du serveur connecté
     */
    public static TMServer getServer() {
        return server;
    }
}
